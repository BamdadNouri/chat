const express = require("express")
const app = express()
const server = require("http").Server(app)
const io = require("socket.io")(server)
const mongoose = require("mongoose")
const Message = require("./models/message")
const bodyParser = require('body-parser')
require("dotenv").config()

server.listen(3600, () => "Running on 3600")

mongoose.connect('mongodb://localhost/awesomeChat', { useNewUrlParser: true, useUnifiedTopology: true })
  .then(() => console.log('Connected to MongoDB...'))
  .catch(err => console.error('Could not connect to MongoDB...'));

const tech = io.of("/tech")
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }))
app.get("/", (req, res) => {
  res.sendFile(__dirname + "/public/index.html")
})
app.get("/chat", (req, res) => {
  res.sendFile(__dirname + "/public/chat.html")
})

app.get("/messages/:room", async (req, res) => {
  const msgs = await Message.find({ room: req.params.room })
  res.send(msgs)
})
async function saveService(data) {
  // if (data.name === undefined || data.message === undefined) {
  //   return res.status(400).send("Bad request!")
  // }
  let newMessage = new Message({ name: data.name, body: data.body, room: data.room })
console.log("nm", newMessage)
  newMessage = await newMessage.save()
console.log("HERE", newMessage)  
return newMessage
}


tech.on("connection", socket => {
  socket.on("join", data => {
    socket.join(data.room)
    tech.in(data.room).emit("message", { body: `Someone just joined to ${data.room}`, room: data.room, name: data.name })
  })

  socket.on("message", data => {
console.log("messssage")
    saveService(data)
      .then(res => {
        tech.in(res.room).emit("message", res)
      })
      .catch(err => {
        console.log("FUCK!!");
      })
  })
})
