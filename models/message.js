const mongoose = require("mongoose")

const Message = mongoose.model("Messsage", new mongoose.Schema({
  name: {
    type: String
  },
  body: {
    type: String
  },
  room: {
    type: String
  }
}))

module.exports = Message
